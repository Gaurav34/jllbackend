

const mongoose = require("mongoose");

const TimeSchema = new mongoose.Schema(
  {
    pageid:   String,
    time:   Number,
    userEmail:   String,
  },
  {
    timestamps: true
  }
);

const TimerecordModal = mongoose.model("timerecords", TimeSchema);
module.exports = TimerecordModal;
