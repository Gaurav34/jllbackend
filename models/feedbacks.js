const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Feedback = new Schema(
  {
    userEmail: String,
    userName: String,
   
    ques1: String,
    ques2: String,
    ques3: String,
    ques4: String,
    ques5: String,
    ques6: String,
    ques7: String,
    
   
    
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("feedbacks", Feedback);

// import * as mongoose from "mongoose";

// export interface usersModelTypes extends mongoose.Document {
//   name: String;
//   age: Number;
//   email: String;
//   status: Boolean;
//   phoneNumber: Number;
// }

// export const usersModelSchema = new mongoose.Schema(
//   {
//     name: { type: String, required: true },
//     IpVoted: { type: Array }
//   },
//   {
//     timestamps: true
//   }
// );

// const userModel = mongoose.model<usersModelTypes>("surveys", usersModelSchema);
// export default userModel;
